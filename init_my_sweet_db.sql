BEGIN
    TRANSACTION;
CREATE TABLE users
(
    id       SERIAL PRIMARY KEY NOT NULL,
    email    VARCHAR(100)       NOT NULL,
    password VARCHAR(100)       NOT NULL,
    username VARCHAR(100)       NOT NULL,
    UNIQUE (email),
    CONSTRAINT username_idx UNIQUE (username)
);
CREATE TABLE video
(
    id             SERIAL PRIMARY KEY NOT NULL,
    name           VARCHAR(100)       NOT NULL,
    count_likes    INTEGER            NOT NULL CHECK (count_likes >= 0),
    count_comments INTEGER            NOT NULL CHECK (count_comments >= 0),
    user_id        INTEGER            NOT NULL,
    link           VARCHAR(100)       NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE RESTRICT
);
CREATE TABLE chat
(
    id        SERIAL PRIMARY KEY NOT NULL,
    user_id_1 INTEGER            NOT NULL,
    user_id_2 INTEGER            NOT NULL,
    message   TEXT               NOT NULL,
    date      DATE               NOT NULL CHECK (date >= '2020-05-03':: date
        ),
    FOREIGN KEY (user_id_1) references users (id) ON DELETE RESTRICT,
    FOREIGN KEY (user_id_2) REFERENCES users (id) ON DELETE RESTRICT
);
CREATE TABLE comments
(
    id         SERIAL PRIMARY KEY NOT NULL,
    user_id    INTEGER            NOT NULL,
    video_id   INTEGER            NOT NULL,
    text       varchar(256)       NOT NULL,
    count_like INTEGER            NOT NULL CHECK (count_like >= 0),
    date       DATE               NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE RESTRICT,
    FOREIGN KEY (video_id) REFERENCES video (id) ON DELETE RESTRICT
);

CREATE MATERIALIZED VIEW favorite_video_materialized AS
SELECT users.id as user_id, video.id as video_id, video.name as vide_name, email, username
FROM users
         LEFT JOIN video ON video.user_id = users.id;
COMMIT;
