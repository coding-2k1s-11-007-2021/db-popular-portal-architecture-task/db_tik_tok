CREATE INDEX ON flights(aircraft_code);
CREATE INDEX ON ticket_flights(flight_id);
CREATE INDEX ON tickets(ticket_no);
CREATE INDEX ON aircrafts_data(aircraft_code);
-- Создаем четыре индекса, чтобы быстрее соединять таблицы, так как
-- соединение таблиц - времязатратно
SET work_mem TO '400MB';
-- Выделяем больше памяти, так как до выделения этой памяти запрос использовал
-- временные файлы на диске, что замедляло его работу
SET enable_hashjoin = off;
-- Выключаем функцию хеша и используем merge join и, смотря по графикам, merge join работает быстрее.
-- Соединение слиянием с использованием индекса показывает небольшой линейный рост стоимости.
-- При достаточном объеме work_mem соединение хешированием обычно эффективнее, но,
-- когда дело доходит до временных файлов, соединение слиянием выигрывает.
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
with
    unique_passengers_per_date_and_aircraft as (
        select
            a.aircraft_code,
            a.model,
            f.scheduled_departure::date as scheduled_departure_date,
            count(distinct t.passenger_id) as unique_passenger_id_cnt
        from aircrafts a
        left join flights f on a.aircraft_code = f.aircraft_code
        left join ticket_flights tf on f.flight_id = tf.flight_id
        left join tickets t on tf.ticket_no = t.ticket_no
        group by
            a.aircraft_code,
            a.model,
            scheduled_departure_date
    )

select
    aircraft_code,
    model,
    avg(unique_passenger_id_cnt) as cnt
from unique_passengers_per_date_and_aircraft
group by aircraft_code, model
order by cnt desc
limit 5;
