CREATE INDEX ON seats(fare_conditions);
-- Создаем индексы, чтобы быстрее соединять таблицы, так как
-- соединение таблиц - очень долгий процесс
SET work_mem TO '200MB';
-- Выделяем больше памяти, так как до выделения этой памяти запрос использовал
-- временные файлы на диске, что замедляло его работу
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
select
    a.aircraft_code,
    a.model,
    count(fare_conditions = 'Business' or NULL) * 100.0 / count(*) as business_cnt
from aircrafts a
left join seats s on a.aircraft_code = s.aircraft_code
group by a.aircraft_code, a.model
order by business_cnt desc
limit 5;
