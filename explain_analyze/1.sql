CREATE INDEX ON flights(arrival_airport);
CREATE INDEX ON airports_data(airport_code);
-- Создаем два индекса, чтобы быстрее соединять таблицы, так как
-- соединение таблиц - очень долгий процесс
SET work_mem TO '200MB';
-- Выделяем больше памяти, так как до выделения этой памяти запрос использовал
-- временные файлы на диске, что замедляло его работу
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
select
    flight_id,
    flight_no,
    departure_airport,
    arrival_airport,
    scheduled_departure,
    scheduled_arrival,
    status,
    case
        when scheduled_arrival::time <= '12:00' then 'before midday'
        else 'after midday'
    end as before_after_midday
from flights f
left join airports a on f.arrival_airport = a.airport_code
where
    scheduled_arrival::date = '2017-08-01'
    and status = 'Arrived'
    and a.city = 'Moscow';
