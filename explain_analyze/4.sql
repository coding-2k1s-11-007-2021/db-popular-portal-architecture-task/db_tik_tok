CREATE INDEX ON flights(departure_airport desc, scheduled_departure desc, flight_id desc);
-- Создали индексы, чтобы эти 3 сортировки происходили быстрее
SET work_mem TO '200MB';
-- Выделяем память, так как до этого использовались временные файлы на диске, которые замедляли работу запроса
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
select
    flight_id,
    flight_no,
    departure_airport,
    scheduled_departure,
    count(status = 'Cancelled' or null) over (partition by departure_airport order by scheduled_departure, flight_id rows between 10 preceding and 1 preceding) > 0 as has_prev10_cancelled
from flights;
